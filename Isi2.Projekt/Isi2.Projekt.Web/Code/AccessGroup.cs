﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Isi2.Projekt.Contracts;
using Isi2.Projekt.Contracts.User;
using Isi2.Projekt.Web.Helpers;

namespace Isi2.Projekt.Web.Code
{
    public class AccessGroupAttribute : ActionFilterAttribute
    {
        private IService _service = WindsorContainerAccess.GetContainer().Resolve<IService>();

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var hash = HttpContext.Current.Request.Params.Get("h");
            if (!string.IsNullOrWhiteSpace(hash))
            {
                var user = _service.GetUsers(new UserFilterContract() {Login = HttpContext.Current.User.Identity.Name}, null);
                if (AccessGroupSecurityProvider.Instance.ValidateHash(user.Items.First().Id, hash))
                {
                    base.OnActionExecuting(filterContext);  
                    return;
                }
               
            }
            RedirectToMainPage(filterContext);
        }

        private void RedirectToMainPage(ActionExecutingContext filterContext)
        {
            RouteValueDictionary routeValues = new RouteValueDictionary(new
                {
                    action = "Index",
                    controller = "Entry",
                });
            filterContext.Result = new RedirectToRouteResult(routeValues);
        }
    }
}